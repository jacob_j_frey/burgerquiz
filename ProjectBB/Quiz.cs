﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBB
{
    public class Quiz
    {
        public Queue<Question> Questions { get; set; } = new Queue<Question>();

        public int TotalPossible { get; set; }

        public int TotalCorrect { get; set; }

        public int Percentage { get
            {
                return (TotalPossible>0)? TotalCorrect*100/TotalPossible: 0;
            } 
        }

        public Boolean AddQuestion(Question question)
        {
            try
            {
                Questions.Enqueue(question);
                TotalPossible++;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void askQuestions()
        {
            int RunningTotal = 0;
            Console.Clear();
            foreach(Question q in Questions)
            {
                RunningTotal++;
                Console.WriteLine($"QUESTION #{RunningTotal}");
                string isCorrect = nextQuestion(q) ? "That is CORRECT!" : $"Sorry, that is INCORRECT! {q.Response}.";
                Console.Clear();
                Console.WriteLine($"\n {isCorrect}");
                Console.WriteLine($"\n You have {TotalCorrect} CORRECT out of {RunningTotal} TOTAL for {TotalCorrect*100/RunningTotal}%!!!\n\n");
            }
            Console.WriteLine($"\n Your FINAL SCORE is {TotalCorrect} out of {TotalPossible} for a PERCENTAGE of {Percentage}%\n");
        }

        public bool nextQuestion(Question question)
        {
            Console.WriteLine($" {question.Query}");
            String input = Console.ReadLine();
            while (!question.ValidateInput(input))
            {
                Console.WriteLine("Sorry, that is not a valid answer, please try again:");
                input = Console.ReadLine();
            }
            if (question.ValidateAnswer(input)) TotalCorrect++;
            return (question.ValidateAnswer(input));
        }
    }
}
