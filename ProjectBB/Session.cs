﻿using ProjectBB.DAOs;
using ProjectBB.Questions;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBB
{
  
    public class Session
    {
        public Quiz Quiz = new Quiz();

        private List<Episode> RawData { get; set; }

        private int TotalSessionQ { get; set; } = 0;

        private int CorrectSessionQ { get; set; }  = 0;

        private int Percent { get
            {
                return (CorrectSessionQ * 100 / TotalSessionQ);
            }
        }

        //Offline data repository
        //Reader test = new Reader();
        //List<Episode> rawData = test.GetData();

        public Session()
        {
            SQLEpisodeDAO DAOEpisode = new SQLEpisodeDAO();
            RawData = DAOEpisode.GetEpisodeData();
        }

        public void RunSession()
        {
            String again = "Y";
            while (again == "Y")
            {   
                CreateQuiz();
                RunQuiz();

                Console.WriteLine($" For the CURRENT SESSION, you have correctly answered {CorrectSessionQ} out of {TotalSessionQ} for a percent of {Percent}%");
                Console.WriteLine("\n\n Would you like to take another quiz [Y/N]?");
                again = Console.ReadLine().ToUpper().Trim().Substring(0, 1);
                Console.Clear();
            }
            
        }


        public Boolean CreateQuiz()
        {
            Quiz = new Quiz();
            Console.WriteLine("\n Welcome to Bob Burger's Quiz!\n\n How many questions would you like?");
            String userinput = Console.ReadLine();
            while (!(int.Parse(userinput) > 0))
            {
                Console.WriteLine("Sorry, it needs to be a number greater than 0!");
                userinput = Console.ReadLine();
            }
            int noOfQuestions = int.Parse(userinput);
            if (noOfQuestions > 20)
            {
                Console.WriteLine("Whoa there buddy, that's a lot of questions. I am going to start you out with 20 questions.");
                noOfQuestions = 20;
            } 
            if (noOfQuestions == 20)
            {
                Console.WriteLine("There is a special reward if you get all 20 CORRECT!");
            }
            for (int i = 0; i < noOfQuestions; i++)
            {
                Question question;
                Random random = new Random();
                int rand = random.Next(1, 6);
              
                if (rand == 1) question = new QIndexEpisode(RawData);
                else if (rand == 2) question = new QEpisodeTF(RawData);
                else if (rand == 3) question = new QStorefrontM(RawData);
                else if (rand == 4) question = new QMExterminator(RawData);
                else question = new QNameEpisode(RawData);
                
                Quiz.AddQuestion(question);
            }
            Console.WriteLine("\n\n Press any key to start quiz...");
            Console.ReadKey();
            return (Quiz.TotalPossible > 0) ? true : false;
        }

        public Boolean RunQuiz()
        {
            //Console.Clear();
            Quiz.askQuestions();

            if (Quiz.Percentage == 100 && Quiz.TotalPossible == 20)
            {
                Reader nr = new Reader();
                nr.ShowBob();
            }

            TotalSessionQ += Quiz.TotalPossible;
            CorrectSessionQ += Quiz.TotalCorrect;


            return true;
        }
    
    }
}
