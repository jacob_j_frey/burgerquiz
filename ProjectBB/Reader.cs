﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection.Metadata;

namespace ProjectBB
{
    public class Reader
    {
        private string File { get; set; } = @"BobsBurgers.txt";

        private string Bob { get; set; } = @"bob.txt";

        ///
        ///Summary:
        ///      Gets Data from the file and pulls it into the program
        ///
        public List<Episode> GetData()
        {
            List<Episode> episodeList = new List<Episode>();
            Console.WriteLine($"\n Retrieving Questions from the File\n");
            using StreamReader sr = new StreamReader(File);
            try
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    Episode tempEpisode = new Episode(line);
                    episodeList.Add(tempEpisode);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error Trace: {e}");
            }
            return episodeList;
        }

        public void ShowBob()
        {
            using StreamReader sr = new StreamReader(Bob);
            try
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error Trace: {e}");
            }
        }

        public void currentTime()
        {
            using StreamWriter sr = new StreamWriter("LogTime.txt");
            try
            {
                sr.Write(DateTime.Now.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error Trace: {e}");
            }
        }
    }


}
