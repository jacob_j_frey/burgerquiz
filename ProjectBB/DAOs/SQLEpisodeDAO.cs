﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace ProjectBB.DAOs
{
    public class SQLEpisodeDAO
    {
        private string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog = BurgerData; Integrated Security = True";

        public List<Episode> GetEpisodeData()
        {
            List<Episode> output = new List<Episode>();

            try{
                SqlConnection sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                SqlCommand sqlCommand = new SqlCommand();
                string sqlQuery = $"SELECT * FROM episodes;";
                sqlCommand.CommandText = sqlQuery;
                sqlCommand.Connection = sqlConnection;
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while(reader.Read())
                {
                    Episode episode = new Episode();
                    episode = ConvertEpisode(reader);
                    //episode.Index = Convert.ToInt32(reader["index"]);
                    //episode.EpisodeNo = Convert.ToInt32(reader["episode"]);
                    //episode.Title = Convert.ToString(reader["title"]);
                    //episode.Season = Convert.ToInt32(reader["season"]);
                    //episode.Van = Convert.ToString(reader["van"]);
                    //episode.Storefront = Convert.ToString(reader["storefront"]);
                    //episode.Sign = Convert.ToString(reader["sf_sign"]);
                    output.Add(episode);
                }
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                throw;
            }
            return output;
        }

        private Episode ConvertEpisode(SqlDataReader reader)
        {
            Episode episode = new Episode();
            episode.Index = Convert.ToInt32(reader["index"]);
            episode.EpisodeNo = Convert.ToInt32(reader["episode"]);
            episode.Title = Convert.ToString(reader["title"]);
            episode.Season = Convert.ToInt32(reader["season"]);
            episode.Van = Convert.ToString(reader["van"]);
            episode.Storefront = Convert.ToString(reader["storefront"]);
            episode.Sign = Convert.ToString(reader["sf_sign"]);
            return episode;
        }
    }
}
