﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ProjectBB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"      ▄▄▄▄        ▄▄▄▄    ▄▄▄▄      ▄▄▄   ▄▄   ▄▄▄  ▄▄▄ ▐ ▄▄      ▄▄▄       ▄▄        ");
            Console.WriteLine(@"     ▐█ ▀█  ▄█▀▄ ▐█ ▀█   ▐█ ▀█ █ ██▌█▄ █ ▐█ ▀  ▐▄ ▀ █▄ █ ▐█ ▀    ▐▀ ▀█ █ ██▌▄▄  ▄▀▀▀█▌");
            Console.WriteLine(@"     ▐█▀▀█▄▐█▌ ▐▌▐█▀▀█▄  ▐█▀▀█▄▐▌▐█▌█▀█▄ ▄█ ▀█▄▐▀▀  █▀█▄  ▀▀▀█▄  █▌  █▌▐▌▐█▌▐█    ▄█▀ ");
            Console.WriteLine(@"     ██▄ ▐█▐█▌ ▐▌██▄ ▐█  ██▄ ▐█▐█▄█▌█  █▌▐█▄ ▐█▐█▄▄▌█  █▌▐█▄ ▐█  ▐█ ▄█ ▐█▄█▌▐█▌ ▄█▀   ");
            Console.WriteLine(@"      ▀▀▀▀  ▀█▄▀  ▀▀▀▀    ▀▀▀▀  ▀▀▀ ▀   ▀ ▀▀▀▀  ▀▀▀ ▀   ▀ ▀▀▀▀    ▀▀█▄  ▀▀▀ ▀▀▀ ██▄▄▀ ");


            Session current = new Session();
            current.RunSession();

            System.Environment.Exit(0);
        }
    }
}
