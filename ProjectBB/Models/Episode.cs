﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace ProjectBB
{
    public class Episode
    {
        public int Index { get; set; }

        public int Season { get; set; }

        public int EpisodeNo { get; set; }

        public string Title { get; set; }

        public string Identifier => $"S{Season}E{EpisodeNo}";

        public string Van { get; set; }

        public string Storefront { get; set; }

        public string Sign { get; set; }

        public Episode()
        {

        }

        public Episode(int index, int season, int episode, string title, string van, string storefront, string sign)
        {
            Index = index;
            Season = season;
            EpisodeNo = episode;
            Title = title;
            Van = van;
            Storefront = storefront;
            Sign = sign;
        }

        public Episode(string line)
        {
            string[] episodeStream = line.Split('|');
            try
            {
                Index = int.Parse(episodeStream[0]);
                Season = int.Parse(episodeStream[1]);
                EpisodeNo = int.Parse(episodeStream[2]);
                Title = episodeStream[3];
            } 
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public override string ToString()
        {
            return (Identifier.PadLeft(6) + $": " + Title);
        }
    }

}
