﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace ProjectBB.Models
{
    public class User
    {
        public int UserId {get; set;}

        public String Username { get; set; }

        private String HashPassword { get; set; }

        private double Points { get; set; }

        private int TotalQuestions { get; set; }

        private int CorrectQuestions { get; set; }

        public User()
        {

        }

        public double addPts(double points)
        {
            Points += points;
            return Points;
        }

        public int updateTotals(int totalPossible, int totalCorrect)
        {
            TotalQuestions += totalPossible;
            CorrectQuestions += totalCorrect;
            return (CorrectQuestions * 100 / TotalQuestions);
        }
    }
}
