﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBB.Questions
{
    public class QStorefrontM : Question

    {
        public QStorefrontM(List<Episode> data)
        {
            Type = "Multiple Choice";
            Random rand = new Random();
            int index1 = rand.Next(data.Count);
            int index2 = rand.Next(data.Count);
            int index3 = rand.Next(data.Count);
            while (index1 == index2)
            {
                index2 = rand.Next(data.Count);
            }
            while (index1 == index3 || index2 == index3)
            {
                index3 = rand.Next(data.Count);
            }
            Episode answer = data[index1];
            Episode alt1 = data[index2];
            Episode alt2 = data[index3];
            List<Episode> list = new List<Episode>();
            list.Add(answer);
            //adds the first one as the answer
            Answer = answer.Identifier;
            String episodeStorefront = answer.Storefront;
            Response = $"{answer.Storefront} was from Episode {answer.Identifier} {answer.Title}";
            list.Add(alt1);
            list.Add(alt2);
            list = RandomizeList(list);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Identifier == Answer)
                {
                    Answer = "" + (i + 1);
                }
            }
            Query = $"Which is the Season and Episode that had {episodeStorefront} as the storefront next door? \n 1. {list[0].Title} \n 2. {list[1].Title} \n 3. {list[2].Title} \n Choose Correct Number: ";

        }

        public override bool ValidateAnswer(string answer)
        {
            return (answer == Answer);
        }

        public override bool ValidateInput(string answer)
        {
            return answer == "1" || answer == "2" || answer == "3";
        }

        private List<Episode> RandomizeList(List<Episode> list)
        {
            Stack<Episode> stack = new Stack<Episode>();
            while (list.Count > 0)
            {
                Random rand = new Random();
                int index = rand.Next(list.Count);
                stack.Push(list[index]);
                list.RemoveAt(index);

            }
            while (stack.Count > 0)
            {
                list.Add(stack.Pop());
            }
            return list;
        }
    }
}
