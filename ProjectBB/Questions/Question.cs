﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBB
{
    public abstract class Question
    {
        public string Type { get; set; }
        
        public string Query { get; set; }

        public string Answer { get; set; }

        public string Response { get; set; }
        
        public abstract Boolean ValidateAnswer(String answer);

        public abstract Boolean ValidateInput(String answer);
    }
}
