﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBB
{
    class QEpisodeTF : Question
    {


        public QEpisodeTF(List<Episode> data)
        {
            Type = "Boolean";
            //Create Answer and Question from data 
            Random rand = new Random();
            int index1 = rand.Next(data.Count);
            int index2 = rand.Next(data.Count);
            while (index1==index2)
            {
                index2 = rand.Next(data.Count);
            }
            Episode one = data[index1];
            Episode two = data[index2];
            Answer = (one.Index < two.Index) ? "TRUE" : "FALSE";
            Query = $"True or False: The Episode  '{one.Title}'  came before  '{two.Title}' ";
            Response = $"{one.Title} came after {two.Title}";
        }

        public override Boolean ValidateAnswer(String answer)
        {
            answer = answer.ToUpper();
            if (answer == "T" || answer == "F")
            {
                answer = (answer == "T") ? "TRUE" : "FALSE";
            }
            return (answer == Answer);
        }

        public override Boolean ValidateInput(String answer)
        {
            answer = answer.ToUpper();
            if (answer == "T" || answer == "F")
            {
                answer = (answer == "T") ? "TRUE" : "FALSE";
            }
            if (answer == "TRUE" || answer == "FALSE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
